from src.lab_2 import lab2
import pytest

# Параметры тестов для бинарного поиска
test_param_for_binary_search = [
    ([0, 0, 1, 1, 2, 3, 3, 3, 3, 3, 4, 5, 5, 6, 7, 8, 8, 9], 3, [6, 7, 8, 9, 10]),
    (['A', 'S', 'A', 'A', 'B', 'S', 'C', 'C', 'D', 'E', 'I', 'K', 'K', 'N', 'O', 'P', 'R', 'V'], 'A', [1, 2, 3, 4]),
    ([0, 2, 3, 7, 11, 12, 14, 15, 16, 22, 34, 41, 56, 78, 92], 34, [11]),
    ([0, 1, 2, 5, 7, 9, 13], 100, False)
]

# Параметры тестов наивного поиска и поиска Кнута-Морриса-Пратта
test_param_for_search_in_line = [
    ('ABCDHQABCGGGHAWBDHACBCBAABABCDDH', 'ABC', [1, 7, 27]),
    ('abcababcabdcaapabcaacababcabdwaabcacabcaabcabcaodabcaad', 'abcabd', [6, 24, 44]),
    ('3HEHELL8334HEL932HHELLO123iHLEBLLqjfuewHhf9HELLO173eifHjjf94f', 'HELLO123', [19, 44]),
    ('abcabcabcabcdabcdabcaccabbccaabcdacb', 'abd', False)
]

# Параметры тестов для линейного поиска
test_param_for_linear_search = [
    ([0, 3, 8, 5, 9, 3, 3, 0, 1, 5, 4, 7, 3, 6, 8, 3, 1, 2], 3, [2, 6, 7, 13, 16]),
    (['A', 'B', 'C', 'D', 'E', 'R', 'K', 'A', 'A', 'V', 'A', 'C', 'B', 'N', 'K', 'I', 'O', 'P'], 'A', [1, 8, 9, 11]),
    ([12, 14, 92, 34, 56, 78, 41, 3, 2, 11, 0, 15, 16, 22, 7], 0, [11]),
    ([1, 5, 7, 2, 9, 0, 13], 100, False)
]


# Тест линейного поиска
@pytest.mark.parametrize("mass_in, var, mass_out", test_param_for_linear_search)
def test_linear_search(mass_in, var, mass_out):  # на корректность поиска
    assert lab2.linear(mass_in, var) == mass_out


def test_linear_search_data():  # на корректность данных
    with pytest.raises(TypeError) as e:
        lab2.linear(7146235, 6)
    assert 'the value should be a list' in str(e)


# Тест бинарного поиска
@pytest.mark.parametrize("mass_in, var, mass_out", test_param_for_binary_search)
def test_binary_search(mass_in, var, mass_out):  # на корректность поиска
    assert lab2.binary(mass_in, var) == mass_out


def test_binary_search_data():  # на корректность данных
    with pytest.raises(TypeError) as e:
        lab2.binary(1234567, 6)
    assert 'the value should be a list' in str(e)


# Тест наивного поиска
@pytest.mark.parametrize("line_in, var, mass_out", test_param_for_search_in_line)
def test_naive_search(line_in, var, mass_out):  # на корректность поиска
    assert lab2.naive(line_in, var) == mass_out


def test_naive_search_data():  # на корректность данных
    with pytest.raises(TypeError) as e:
        lab2.naive(['f', 'g', 'h', 'd', 'h', 'f', 'f', 'd'], 'f')
    assert 'the value should be a str' in str(e)


# Тест поиска Кнута-Морриса-Пратта
@pytest.mark.parametrize("line_in, var, mass_out", test_param_cases_for_search_in_line)
def test_KMP_search(line_in, var, mass_out):  # на корректность поиска
    assert lab2.KMP(line_in, var) == mass_out


def test_KMP_search_data():  # на корректность данных
    with pytest.raises(TypeError) as e:
        lab2.KMP(['fghdhffd'], ['f'])
    assert 'the value should be a str' in str(e)
