from src.kr_4 import kr4
import pytest

# Параметры для тестов Median фильтра
# Значения которые должны быть получены были округлены
test_param_cases_Median_fltr = [
    ([2, 4, 8, 9, 0, 1, 7, 6, 3, 5], 3, [4, 8, 8, 1, 1, 6, 6, 5]),
    ([12, 4, 8, 3, 11, 10, 23, 14, 6, 15, 17, 18, 9, 5], 5, [8, 8, 10, 11, 11, 14, 15, 15, 15, 15]),
    ([101, 32, 45, 54, 61, 58, 100, 81, 94, 91, 21, 23, 42, 31, 30, 10, 12], 7,
     [58, 58, 61, 81, 81, 81, 81, 42, 31, 30, 23])
]


# Параметры для тестов SMA
# Значения которые должны быть получены были округлены
test_param_cases_SMA = [
    ([2, 4, 8, 9, 0, 1, 7, 6, 3, 5], 3, [4.7, 7.0, 5.7, 3.3, 2.7, 4.7, 5.3, 4.7]),
    ([12, 4, 8, 3, 11, 10, 23, 14, 6, 15, 17, 18, 9, 5], 5, [7.6, 7.2, 11.0, 12.2, 12.8, 13.6, 15.0, 14.0, 13.0, 12.8]),
    ([101, 32, 45, 54, 61, 58, 100, 81, 94, 91, 21, 23, 42, 31, 30, 10, 12], 7,
     [64.4, 61.6, 70.4, 77.0, 72.3, 66.9, 64.6, 54.7, 47.4, 35.4, 24.1])
]


# Параметры для тестов EMA
# Значения которые должны быть получены были округлены
test_param_cases_EMA = [
    ([2, 4, 8, 9, 0, 1, 7, 6, 3, 5], 3, [4.7, 9.0, 0.0, 1.0, 7.0, 6.0, 3.0, 5.0]),
    ([12, 4, 8, 3, 11, 10, 23, 14, 6, 15, 17, 18, 9, 5], 5, [7.6, 8.8, 15.9, 14.9, 10.4, 12.7, 14.8, 16.4, 12.7, 8.8]),
    ([101, 32, 45, 54, 61, 58, 100, 81, 94, 91, 21, 23, 42, 31, 30, 10, 12], 7,
     [64.4, 69.9, 77.9, 82.3, 61.9, 48.9, 46.6, 41.4, 37.6, 28.4, 22.9])
]


# Тест Median фильтра
@pytest.mark.parametrize("mass_in, var, mass_out", test_param_cases_Median_fltr)
def test_Median_fltr(mass_in, var, mass_out):
    assert kr4.Median_fltr(mass_in, var) == mass_out


# Тест SMA
@pytest.mark.parametrize("mass_in, var, mass_out", test_param_cases_SMA)
def test_SMA(mass_in, var, mass_out):
    assert kr4.SMA(mass_in, var) == mass_out


# Тест EMA
@pytest.mark.parametrize("mass_in, var, mass_out", test_param_cases_EMA)
def test_EMA(mass_in, var, mass_out):
    assert kr4.EMA(mass_in, var) == mass_out
