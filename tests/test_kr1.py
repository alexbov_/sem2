from src.kr_1 import kr1
import pytest

# Параметры для тестов методов: прямоугольников, трапеций, Симпсона
# Значения которые должны быть получены были округлены
test_param_cases = [
    (3, 0, 100, 10, 851, 891, 894),
    (5, 4, 48, 6, 301, 328, 332),
    (-2, -50, 50, 24, 766, 766, 768)
]


# Тест интегрирования методом прямоугольников
@pytest.mark.parametrize("a, x1, x2, q, S1, S2, S3", test_param_cases)
def test_rctngl(a, x1, x2, q, S1, S2, S3):
    assert kr1.rctngl(a, x1, x2, q) == S1


# Тест интегрирования методом трапеций
@pytest.mark.parametrize("a, x1, x2, q, S1, S2, S3", test_param_cases)
def test_trap(a, x1, x2, q, S1, S2, S3):
    assert kr1.trap(a, x1, x2, q) == S2


# Тест интегрирования методом Симпсона
@pytest.mark.parametrize("a, x1, x2, q, S1, S2, S3", test_param_cases)
def test_simpson(a, x1, x2, q, S1, S2, S3):
    assert kr1.simpson(a, x1, x2, q) == S3
