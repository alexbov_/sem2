def Bubble_sort_rise(a, b):
    if a[1] > b[1]:
        a, b = b, a
    return a, b


def Bubble_sort_decr(a, b):
    if a[1] < b[1]:
        a, b = b, a
    return a, b


# Пузырьковая сортировка
def Bubble_sort(A, reverse=False):
    if type(A) not in [list]:
        raise TypeError('the value should be a list')
    var = 1
    for _ in range(len(A) - 1):
        for i in range(len(A) - var):
            if not reverse:
                # на возрастание
                A[i], A[i + 1] = Bubble_sort_rise(A[i], A[i + 1])
            else:
                # на убывание
                A[i], A[i + 1] = Bubble_sort_decr(A[i], A[i + 1])
        var += 1
    return A


def Sort_by_inserts_rise(a, b, i):
    if a[1] < b[1]:
        a, b = b, a
        i -= 1
        if i == 0:
            return a, b, i, False
    else:
        return a, b, i, False
    return a, b, i, True


def Sort_by_inserts_decr(a, b, i):
    if a[1] > b[1]:
        a, b = b, a
        i -= 1
        if i == 0:
            return a, b, i, False
    else:
        return a, b, i, False
    return a, b, i, True


# Сортировка вставками
def Sort_by_inserts(A, reverse=False):
    if type(A) not in [list]:
        raise TypeError('the value should be a list')
    for i in range(1, len(A)):
        while True:
            if not reverse:
                # на возрастание
                A[i], A[i - 1], i, var = Sort_by_inserts_rise(A[i], A[i - 1], i)
                if not var:
                    break
            else:
                # на убывание
                A[i], A[i - 1], i, var = Sort_by_inserts_decr(A[i], A[i - 1], i)
                if not var:
                    break
    return A


def Shell_sort_rise(a, b):
    if a[1] < b[1]:
        a, b = b, a
    return a, b


def Shell_sort_decr(a, b):
    if a[1] > b[1]:
        a, b = b, a
    return a, b


# Сортировка Шелла
def Shell_sort(A, reverse=False):
    if type(A) not in [list]:
        raise TypeError('the value should be a list')
    step = len(A) // 2
    while True:
        for i in range(step, len(A)):
            while i - step >= 0:
                if not reverse:
                    # на возрастание
                    A[i], A[i - step] = Shell_sort_rise(A[i], A[i - step])
                else:
                    # на убывание
                    A[i], A[i - step] = Shell_sort_decr(A[i], A[i - step])
                i -= step
        step = step // 2
        if step == 0:
            break
    return A


def Quick_sort_rise(a, M, i, id_M):
    if a[1] < M or (a[1] == M and i != id_M and i < id_M):
        return True
    elif a[1] > M or (a[1] == M and i != id_M and i > id_M):
        return False
    else:
        return None


def Quick_sort_decr(a, M, i, id_M):
    if a[1] > M or (a[1] == M and i != id_M and i < id_M):
        return True
    elif a[1] < M or (a[1] == M and i != id_M and i > id_M):
        return False
    else:
        return None


# Быстрая сортировка
def Quick_sort(A, reverse=False):
    if type(A) not in [list]:
        raise TypeError('the value should be a list')
    if len(A) <= 1:
        return A
    else:
        M, L, R = A[len(A) // 2][1], [], []
        for i in range(len(A)):
            if not reverse:
                # на возрастание
                var = Quick_sort_rise(A[i], M, i, len(A) // 2)
                if var is not None:
                    if var:
                        L.append(A[i])
                    else:
                        R.append(A[i])
            else:
                # на убывание
                var = Quick_sort_decr(A[i], M, i, len(A) // 2)
                if var is not None:
                    if var:
                        L.append(A[i])
                    else:
                        R.append(A[i])
        return Quick_sort(L, reverse) + [A[len(A) // 2]] + Quick_sort(R, reverse)
