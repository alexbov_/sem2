import lab1
import random
from timeit import default_timer as timer
import matplotlib.pyplot as plt
import matplotlib.ticker as tcr
from memory_profiler import memory_usage
import cProfile
import pstats
from pstats import SortKey

def main_1(m):
    mem_usage = memory_usage((lab1.Bubble_sort, (m,),), timestamps=False, interval=1)
    return sum(mem_usage) / len(mem_usage)


def main_2(m):
    mem_usage = memory_usage((lab1.Sort_by_inserts, (m,),), timestamps=False, interval=1)
    return sum(mem_usage) / len(mem_usage)


def main_3(m):
    mem_usage = memory_usage((lab1.Shell_sort, (m,),), timestamps=False, interval=1)
    return sum(mem_usage) / len(mem_usage)


def main_4(m):
    mem_usage = memory_usage((lab1.Quick_sort, (m,),), timestamps=False, interval=1)
    return sum(mem_usage) / len(mem_usage)


# создание массива с массивами рандомных чисел
mass = [[(_, random.randint(-1000, 1000)) for _ in range(i)] for i in range(2000, 12001, 1000)]

stop_time_1, stop_time_2, stop_time_3, stop_time_4 = 0, 0, 0, 0
mass_time_1, mass_time_2, mass_time_3, mass_time_4 = [], [], [], []
size_1, size_2, size_3, size_4 = 0, 0, 0, 0
mass_size_1, mass_size_2, mass_size_3, mass_size_4 = [], [], [], []
mass_qnt_iterations_1, mass_qnt_iterations_2, mass_qnt_iterations_3, mass_qnt_iterations_4 = [], [], [], []

# Заносим кол-во элементов участвующих в сортировке для построения графика
mass_qntin = [len(mass[_]) for _ in range(len(mass))]

# Ширина будущих столбцов в гистограммах
wdt = 1000 / 6

# Заносим кол-во элементов с небольшой корректировкой для построения гистограмм
mass_qnt_in_bar_1 = [len(mass[_]) - (wdt + wdt / 2) for _ in range(len(mass))]
mass_qnt_in_bar_2 = [len(mass[_]) - wdt / 2 for _ in range(len(mass))]
mass_qnt_in_bar_3 = [len(mass[_]) + wdt / 2 for _ in range(len(mass))]
mass_qnt_in_bar_4 = [len(mass[_]) + (wdt + wdt / 2) for _ in range(len(mass))]

if __name__ == "__main__":
    for elem in mass:
        print('Замер времени для массива из', len(elem), 'чисел...')
        start_time_1 = timer()  # Замер времени работы
        lab1.Bubble_sort(elem)  # Пузырьковая сортировка
        mass_time_1.append(timer() - start_time_1)  # Заносим время в массив

        start_time_2 = timer()  # Замер времени работы
        lab1.Sort_by_inserts(elem)  # Сортировка вставками
        mass_time_2.append(timer() - start_time_2)  # Заносим время в массив

        start_time_3 = timer()  # Замер времени работы
        lab1.Shell_sort(elem)  # Сортировка Шелла
        mass_time_3.append(timer() - start_time_3)  # Заносим время в массив

        start_time_4 = timer()  # Замер времени работы
        lab1.Quick_sort(elem)  # Быстрая сортировка
        mass_time_4.append(timer() - start_time_4)  # Заносим время в массив

        print('Вычисление объёма памяти...')
        mass_size_1.append(main_1(elem))  # Замер использованной памяти
        mass_size_2.append(main_2(elem))
        mass_size_3.append(main_3(elem))
        mass_size_4.append(main_4(elem))

        print('Подсчет количества операций...')
        var = 0
        while True:
            if var == 0:
                cProfile.run('lab1.Bubble_sort(elem)', 'stats.log')
            elif var == 1:
                mass_qnt_iterations_1.append(int(qntiter[0].split()[0]))
                cProfile.run('lab1.Sort_by_inserts(elem)', 'stats.log')
            elif var == 2:
                mass_qnt_iterations_2.append(int(qntiter[0].split()[0]))
                cProfile.run('lab1.Shell_sort(elem)', 'stats.log')
            elif var == 3:
                mass_qnt_iterations_3.append(int(qntiter[0].split()[0]))
                cProfile.run('lab1.Quick_sort(elem)', 'stats.log')

            with open('output.txt', 'w') as log_file_stream:
                p = pstats.Stats('stats.log', stream=log_file_stream)
                p.strip_dirs().sort_stats(SortKey.NAME == 'swap').print_stats()

            with open('output.txt') as f:
                lines = [line.strip() for line in f]

            qntiter = [line for line in lines if line not in '' and line.count('swap')]

            var += 1

            if var == 4:
                mass_qntiter_4.append(int(qnt_iterations[0].split()[0]))
                break

        print('---')

    print('Строим график...')

    # График времени
    plt.figure(figsize=(18, 6))
    ax = plt.subplot(131)
    plt.plot(mass_qntin, mass_time_1, marker='o', ms=3, label='Bubble_sort')
    plt.plot(mass_qntin, mass_time_2, marker='o', ms=3, label='Sort_by_inserts')
    plt.plot(mass_qntin, mass_time_3, marker='o', ms=3, label='Shell_sort')
    plt.plot(mass_qntin, mass_time_4, marker='o', ms=3, label='Quick_sort')
    plt.title('График времени')
    plt.xlabel('Кол-во чисел')
    plt.ylabel('Время (с)')
    plt.legend(fontsize=8, ncol=2, facecolor='white', edgecolor='black', title='Сортировки', title_fontsize='8')
    ax.set_xlim(mass_qntin[0], mass_qntin[-1])
    ax.xaxis.set_major_locator(tcr.MultipleLocator(2000))
    plt.grid()

    # График кол-ва операций
    ax = plt.subplot(132)
    plt.plot(mass_qntin, mass_qntiter_1, marker='o', ms=3)
    plt.plot(mass_qntin, mass_qntiter_2, marker='o', ms=3)
    plt.plot(mass_qntin, mass_qntiter_3, marker='o', ms=3)
    plt.plot(mass_qntin, mass_qntiter_4, marker='o', ms=3)
    plt.title('График кол-ва операций')
    plt.xlabel('Кол-во чисел')
    plt.ylabel('Кол-во операций')
    ax.xaxis.set_major_locator(tcr.MultipleLocator(2000))
    plt.grid()

    # График памяти
    ax = plt.subplot(133)
    plt.bar(mass_qnt_in_bar_1, mass_size_1, width=wdt)
    plt.bar(mass_qnt_in_bar_2, mass_size_2, width=wdt)
    plt.bar(mass_qnt_in_bar_3, mass_size_3, width=wdt)
    plt.bar(mass_qnt_in_bar_4, mass_size_4, width=wdt)
    plt.title('График памяти')
    plt.xlabel('Кол-во чисел')
    plt.ylabel('Память (байт)')
    ax.xaxis.set_major_locator(tcr.MultipleLocator(2000))
    plt.grid()

    plt.show()  # Вывод графика
