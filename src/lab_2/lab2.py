def sw_linear(a, b):
    if a == b:
        return True
    return False


# Линейный поиск в структуре данных
def linear(mass_in, var):
    if type(mass_in) not in [list]:
        raise TypeError('the value should be a list')
    mass_out = []
    for i in range(0, len(mass_in)):
        if sw_linear(mass_in[i], var):
            mass_out.append(i + 1)
        # if mass_in[i] == var:
        #     mass_out.append(i + 1)
    if len(mass_out) == 0:
        return False
    else:
        return mass_out


def sw_binary(a, b):
    if a < b:
        return True
    else:
        return False

# Бинарный поиск в структуре данных
def binary(mass_in, var):
    if type(mass_in) not in [list]:
        raise TypeError('the value should be a list')
    mass_out = []
    left = 0
    right = len(mass_in)
    while left != right:
        center = (right + left) // 2
        if sw_binary(mass_in[center], var):
            left = center + 1
        else:
            right = center
    while left < len(mass_in) and mass_in[left] == var:
        mass_out.append(left + 1)
        left += 1
    if len(mass_out) == 0:
        return False
    else:
        return mass_out


def sw_naive_and_KMP(a, b):
    if a == b:
        return True
    else:
        return False

# Наивный поиск в строке
def naive(text, var):
    if type(text) not in [str]:
        raise TypeError('the value should be a str')
    mass_out = []
    for i in range(len(list(text)) - len(list(var)) + 1):
        mass_id = []
        for j in range(len(list(var))):
            if sw_naive_and_KMP(list(text)[i + j], list(var)[j]):
                mass_id.append(i + j + 1)
            else:
                break
        if len(mass_id) == len(list(var)):
            mass_out.append(mass_id[0])
    if len(mass_out) == 0:
        return False
    else:
        return mass_out


# Поиск Кнута-Морриса-Пратта в строке
def KMP(text, var):
    if type(text) not in [str]:
        raise TypeError('the value should be a str')
    mass_out = []
    i = 0
    tmp = 0
    while True:
        mass_id = []
        for j in range(len(list(var))):
            if list(text)[i + j] == list(var)[0] and i == tmp:
                tmp = i + j
            if sw_naive_and_KMP(list(text)[i + j], list(var)[j]):
                mass_id.append(i + j + 1)
            else:
                break
        if len(mass_id) == len(list(var)):
            mass_out.append(mass_id[0])
        if i != tmp:
            i = tmp
        else:
            i += 1
            tmp += 1
        if i > len(list(text)) - len(list(var)):
            break
    if len(mass_out) == 0:
        return False
    else:
        return mass_out
