import lab2
import random
import cProfile
import pstats
from pstats import SortKey
import matplotlib.pyplot as plt
import matplotlib.ticker as tcr

start, stop, step = 2000, 12000, 1000  # Данные для цикла заполнения массивов данных

# Заполненяем массив с массивами рандомных чисел
mass_ms_num = [[random.randint(0, 50) for _ in range(i)] for i in range(start, stop + 1, step)]

# Заполненяем массив рандомными числами из mass_in
mass_var_num = [mass_ms_num[_][random.randint(0, len(mass_ms_num[_]))] for _ in range(len(mass_ms_num))]

mass_ms_str, mass_var_str = [], []

# Заполненяем массив рандомных строк и массива частей строк, которые необходимо найти
for i in range(start, stop + 1, step):
    str_in = ''
    str_var = ''
    for _ in range(int(i / 2)):
        str_in += random.SystemRandom().choice(['ab', 'ac', 'ad', 'bc', 'cd', 'bd'])
    for _ in range(6):
        str_var += random.SystemRandom().choice(['ab', 'ac', 'ad', 'bc', 'cd', 'bd'])
    mass_ms_str.append(str_in)
    mass_var_str.append(str_var)

# Заносим количество элементов, которые участвуют в сортировке для построения графика
mass_qnt_sort = [len(mass_ms_num[_]) for _ in range(len(mass_ms_num))]

mass_qnt_iterations_1, mass_qnt_iterations_2, mass_qnt_iterations_3, mass_qnt_iterations_4 = [], [], [], []

i = 0
print('Подсчет кол-ва операций поиска в структуре данных...')
for elem in mass_ms_num:
    print('Кол-во входных данных:', len(elem))
    var = 0
    while True:
        if var == 0:
            cProfile.run('lab2.linear(elem, mass_var_num[i])', 'stats.log')
        elif var == 1:
            mass_qnt_iterations_1.append(int(qnt_iterations[0].split()[0]))
            cProfile.run('lab2.binary(sorted(elem), mass_var_num[i])', 'stats.log')

        with open('output.txt', 'w') as log_file_stream:
            p = pstats.Stats('stats.log', stream=log_file_stream)
            p.strip_dirs().sort_stats(SortKey.NAME == 'sw').print_stats()

        with open('output.txt') as f:
            lines = [line.strip() for line in f]

        qnt_iterations = [line for line in lines if line not in '' and line.count('sw')]

        var += 1

        if var == 2:
            mass_qnt_iterations_2.append(int(qnt_iterations[0].split()[0]))
            break
    i += 1

i = 0
print('Подсчет кол-ва операций поиска в строке...')
for elem in mass_in_str:
    print('Кол-во входных данных:', len(elem))
    var = 0
    while True:
        if var == 0:
            cProfile.run('lab2.naive(elem, mass_var_str[i])', 'stats.log')
        elif var == 1:
            mass_qnt_iterations_3.append(int(qnt_iterations[0].split()[0]))
            cProfile.run('lab2.KMP(elem, mass_var_str[i])', 'stats.log')

        with open('output.txt', 'w') as log_file_stream:
            p = pstats.Stats('stats.log', stream=log_file_stream)
            p.strip_dirs().sort_stats(SortKey.NAME == 'sw').print_stats()

        with open('output.txt') as f:
            lines = [line.strip() for line in f]

        qnt_iterations = [line for line in lines if line not in '' and line.count('sw')]

        var += 1

        if var == 2:
            mass_qnt_iterations_4.append(int(qnt_iterations[0].split()[0]))
            break
    i += 1

print('Строим график...')

# График кол-ва операций
ax = plt.subplot()
plt.plot(mass_qnt_sort, mass_qnt_iterations_1, marker='o', ms=3, label='Линейный')
plt.plot(mass_qnt_sort, mass_qnt_iterations_2, marker='o', ms=3, label='Бинарный')
plt.plot(mass_qnt_sort, mass_qnt_iterations_3, marker='o', ms=3, label='Наивный')
plt.plot(mass_qnt_sort, mass_qnt_iterations_4, marker='o', ms=3, label='Кнута-Морриса-Пратта')
plt.title('График кол-ва операций')
plt.xlabel('Кол-во чисел')
plt.ylabel('Кол-во операций')
plt.legend(fontsize=8, ncol=2, facecolor='white', edgecolor='black', title='Поиск', title_fontsize='8')
ax.set_xlim(mass_qnt_in[0], mass_qnt_in[-1])
ax.xaxis.set_major_locator(tcr.MultipleLocator(step * 2))
plt.grid()
plt.show()
