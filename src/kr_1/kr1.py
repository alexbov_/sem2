import math


# Функция
def function(a, x):
   # return math.asin(5 * a ** 2 - 9 * a * x + 4 * x ** 2)
   n = 7 * (54 * a ** 2 - 33 * a * x + 4 * x ** 2)
   d = -4 * a ** 2 + a * x + 5 * x ** 2
   if d == 0:
       return None
   return n / d


# Метод прямоугольников
def rctngl(a, x1, x2, q):
    S = 0.
    step = (x2 - x1) / q
    while True:
        S += function(a, x1) * step
        x1 += step
        if x1 + step > x2:
            return round(S + function(a, x1) * (x2 - x1))


# Метод трапеций
def trap(a, x1, x2, q):
    S = 0.
    step = (x2 - x1) / q
    while True:
        S += (function(a, x1) + function(a, x1 + step)) * step / 2
        x1 += step
        if x1 + step > x2:
            return round(S + (function(a, x1) + function(a, x2)) * (x2 - x1) / 2)


# Метод Симпсона
def simpson(a, x1, x2, q):
    summ_1, summ_2 = 0., 0.
    S = function(a, x1) + function(a, x2)
    step = (x2 - x1) / q
    for i in range(1, q):
        if i % 2 != 0:
            summ_1 += function(a, x1 + i * step)
        elif i % 2 == 0 and 2 <= i <= q - 2:
            summ_2 += function(a, x1 + i * step)
    return round((2 * step / 6) * (S + 4 * summ_1 + 2 * summ_2))
