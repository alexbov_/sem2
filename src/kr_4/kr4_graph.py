import pandas as pd
import kr4
import matplotlib.pyplot as plt

# Достаем данные из файла data.xls
mass_val_data = [elem for elem in pd.read_excel('data.xls', usecols='M')[18].tolist() if str(elem) != 'nan']

# Скользящие окна (для Median filter и SMA) / Интервалы сглаживания (для EMA)
window = [31, 53, 77]

# Заполнение массива с данными обработанными Median фильтром
mass_Median_fltr_1 = [None for _ in range(window[0] // 2)]
mass_Median_fltr_2 = [None for _ in range(window[1] // 2)]
mass_Median_fltr_3 = [None for _ in range(window[2] // 2)]

for elem in kr4.Median_fltr(mass_val_data, window[0]):
    mass_Median_fltr_1.append(elem)
for elem in kr4.Median_fltr(mass_val_data, window[1]):
    mass_Median_fltr_2.append(elem)
for elem in kr4.Median_fltr(mass_val_data, window[2]):
    mass_Median_fltr_3.append(elem)

for _ in range(window[0] // 2):
    mass_Median_fltr_1.append(None)
for _ in range(window[1] // 2):
    mass_Median_fltr_2.append(None)
for _ in range(window[2] // 2):
    mass_Median_fltr_3.append(None)

# Заполнение массива с данными обработанными SMA
mass_SMA_1 = [None for _ in range(window[0] // 2)]
mass_SMA_2 = [None for _ in range(window[1] // 2)]
mass_SMA_3 = [None for _ in range(window[2] // 2)]

for elem in kr4.SMA(mass_val_data, window[0]):
    mass_SMA_1.append(elem)
for elem in kr4.SMA(mass_val_data, window[1]):
    mass_SMA_2.append(elem)
for elem in kr4.SMA(mass_val_data, window[2]):
    mass_SMA_3.append(elem)

for _ in range(window[0] // 2):
    mass_SMA_1.append(None)
for _ in range(window[1] // 2):
    mass_SMA_2.append(None)
for _ in range(window[2] // 2):
    mass_SMA_3.append(None)

# Заполнение массива с данными обработанными EMA
mass_EMA_1 = [None for _ in range(window[0] - 1)]
mass_EMA_2 = [None for _ in range(window[1] - 1)]
mass_EMA_3 = [None for _ in range(window[2] - 1)]

for elem in kr4.EMA(mass_val_data, window[0]):
    mass_EMA_1.append(elem)
for elem in kr4.EMA(mass_val_data, window[1]):
    mass_EMA_2.append(elem)
for elem in kr4.EMA(mass_val_data, window[2]):
    mass_EMA_3.append(elem)

# Заполнение массива нумерации значений (ось x на графике)
mass_id = [i + 1 for i in range(len(mass_val_data))]

# Построение графика
ax = plt.subplot(131)
plt.plot(mass_id, mass_val_data, label='Real value')
plt.plot(mass_id, mass_Median_fltr_1, label='Median')
plt.plot(mass_id, mass_SMA_1, label='SMA')
plt.plot(mass_id, mass_EMA_1, label='EMA')
plt.title('Изменение значений (окно: 31)')
plt.xlabel('id')
plt.ylabel('Значение')
plt.legend(fontsize=8, ncol=2, facecolor='white', edgecolor='black', title='Значения', title_fontsize='8')
ax.set_xlim(mass_id[0], mass_id[-1])
plt.grid()

ax = plt.subplot(132)
plt.plot(mass_id, mass_val_data)
plt.plot(mass_id, mass_Median_filter_2)
plt.plot(mass_id, mass_SMA_2)
plt.plot(mass_id, mass_EMA_2)
plt.title('Изменение значений (окно: 53)')
plt.xlabel('id')
plt.ylabel('Значение')
ax.set_xlim(mass_id[0], mass_id[-1])
plt.grid()

ax = plt.subplot(133)
plt.plot(mass_id, mass_val_data)
plt.plot(mass_id, mass_Median_filter_3)
plt.plot(mass_id, mass_SMA_3)
plt.plot(mass_id, mass_EMA_3)
plt.title('Изменение значений (окно: 77)')
plt.xlabel('id')
plt.ylabel('Значение')
ax.set_xlim(mass_id[0], mass_id[-1])
plt.grid()

plt.show()  # Вывод графика
