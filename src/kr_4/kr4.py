# Медианный фильтр
def Median_fltr(mass, var):  # var - скользящее окно (нечетное число)
    mass_new = []
    for i in range(len(mass) - var + 1):
        mass_new.append(sorted([mass[i + j] for j in range(var)])[var // 2])  # рассчет значения
    return mass_new


# SMA - Simple Moving Average (Простая скользящая средняя)
def SMA(mass, var):  # var - скользящее окно
    mass_sma = []
    for i in range(len(mass) - var + 1):
        mass_sma.append(round(sum([mass[i + j] for j in range(var)]) / var, 1))  # рассчет значения SMA
    return mass_sma


# EMA - Exponential Moving Average (Экспоненциальная скользящая средняя)
def EMA(mass, var):  # var - интервал сглаживания
    # весовой коэффициент в интервале от 0 до 1
    a = 2 / (var - 1)
    # как первоначальное значение EMA используется SMA
    mass_ema = [round(sum([mass[_] for _ in range(var)]) / var, 1)]
    for i in range(var, len(mass)):
        # следующие значения EMA рассчитываются по формуле
        mass_ema.append(round(a * mass[i] + (1 - a) * mass_ema[-1], 1))
    return mass_ema
